#pragma once


#include <QObject>
#include <QDebug>
#include <QString>


#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "DataExtention.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PLAG.Plugin" FILE "PluginMeta.json")
	Q_INTERFACES(
	        IPlugin
	)

public:
	Plugin();
	virtual ~Plugin() = default;

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_userTask;
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_dateUserTask;
	DataExtention* m_dataExtention;
};
//!  \}

